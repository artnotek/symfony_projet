<?php

namespace App\DataFixtures;

use App\Entity\Oeuvre;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

class OeuvresFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // instancier faker
        $faker = Faker::create('fr_FR');
        // pour remplir la table, créer des objets puis les persister
        for($i = 0; $i < 30; $i++){
            $product = new Oeuvre();
            $name = $faker->unique()->sentence(4);
            $product
                ->setDescription($faker->text)
                ->setSlug($faker->text)
                ->setImage($faker->image('public/img/oeuvre/', 800, 450, null, false))
                ->setName($name)
                ->setSlug(str_replace(" ", "-", $name))
            ;

            // récupération d'une référence créée dans CategoryFixtures
            $randomCategory = random_int(0, 2);
//            $product->addCategory( $this->getReference("category$randomCategory") );
            $product->setCategory($this->getReference("category$randomCategory"));

            // persist : créer un enregistrement
            $manager->persist($product);
        }

        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
