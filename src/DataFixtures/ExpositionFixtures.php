<?php

namespace App\DataFixtures;

use App\Entity\Expositions;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

class ExpositionFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // instancier faker
        $faker = Faker::create('fr_FR');

        for($i = 0; $i < 30; $i++){
            $product = new Expositions();
            $name = $faker->unique()->sentence(4);
            $product
                ->setDate($faker->unique()->dateTimeBetween('-2 years', '3 years', null))
                ->setLocation($faker->address)
                ->setDescription($faker->text)
            ;

            // récupération d'une référence créée dans CategoryFixtures

            // persist : créer un enregistrement
            $manager->persist($product);
        }



        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
