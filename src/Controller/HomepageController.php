<?php

namespace App\Controller;

use App\Repository\OeuvreRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/", name="homepage.index")
     */

    public function index(Request $request, OeuvreRepository $oeuvreRepository):Response
    {
        $results = $oeuvreRepository->findAll();

        //$response = new Response('projet démarré');
        //return $response;
        return $this->render('homepage/homepage.html.twig', [
            'results' => $results
        ]);
    }
}