<?php

namespace App\Controller;

use App\Repository\ExpositionsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExpositionController extends AbstractController
{
    /**
     * @Route("/expositions", name="exposition.index")
     */
    public function index(ExpositionsRepository $expositionsRepository):Response
    {
        $results = $expositionsRepository->findAll();
        $onlyNextExpo = $expositionsRepository->onlyNextExpo();

        return $this->render('expositions/index.html.twig', [
            'results' => $results,
            'onlyNextExpo' => $onlyNextExpo
        ]);
    }
}