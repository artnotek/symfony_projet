<?php

namespace App\Controller\Admin;

use App\Entity\Expositions;
use App\Form\ExpositionType;
use App\Repository\ExpositionsRepository;
use App\Repository\OeuvreRepository;
use App\Service\FileService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ExpositionController extends AbstractController
{
    /**
     * @Route("/admin/exposition", name="admin.exposition.index")
     */
    public function index(ExpositionsRepository $expositionsRepository):Response
    {
        $result = $expositionsRepository->findAll();
        return $this->render('admin/exposition/index.html.twig', [
            'results' => $result
        ]);
    }

    /**
     * @Route("/admin/exposition/update/{id}", name="admin.exposition.update")
     */
    public function form(Request $request, EntityManagerInterface $entityManager, int $id, ExpositionsRepository $expositionsRepository):Response
    {
        $results = $expositionsRepository->findOneBy([
            'id' => $id
        ]);
        $model = $expositionsRepository->find($id);
        $type = ExpositionType::class;
        $form = $this->createForm($type, $model);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){


            // message de confirmation
            $message = $model->getId() ? "L'exposition a été modifiée" : "L'exposition a été ajoutée";

            // message stocké en session
            $this->addFlash('notice', $message);

            /*
             * insertion dans la base de données
             *  - persist: méthode déclenchée uniquement lors d'une insertion
             *  - lors d'une mise à jour, aucune méthode n'est requise
             *  - remove: méthode déclenchée uniquement lors d'une suppression
             *  - flush: exécution des requêtes SQL
             */
            $model->getId() ? null : $entityManager->persist($model);
            $entityManager->flush();

            // redirection
            return $this->redirectToRoute('admin.exposition.index');
        }
//        dump($request);

        return$this->render('admin/exposition/form.html.twig', [
            'form' => $form->createView(),
            'results' => $results
        ]);
    }
    /**
     * @Route("/admin/exposition/create", name="admin.exposition.create")
     */
    public function create(Request $request, EntityManagerInterface $entityManager, ExpositionsRepository $expositionsRepository):Response
    {
        $results = $expositionsRepository->findAll();
        // si l'id est nul, une insertion est exécutée, sinon une modification est exécutée
        $model = new Expositions();
        $type = ExpositionType::class;
        $form = $this->createForm($type, $model);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {


            // message de confirmation
            $message = "L'exposition a été ajoutée";

            // message stocké en session
            $this->addFlash('notice', $message);

            /*
             * insertion dans la base de données
             *  - persist: méthode déclenchée uniquement lors d'une insertion
             *  - lors d'une mise à jour, aucune méthode n'est requise
             *  - remove: méthode déclenchée uniquement lors d'une suppression
             *  - flush: exécution des requêtes SQL
             */
            $entityManager->persist($model);
            $entityManager->flush();

            // redirection
            return $this->redirectToRoute('admin.exposition.index');
        }
        return$this->render('admin/exposition/create.form.html.twig', [
            'form' => $form->createView()
//            'results' => $results
        ]);
    }
    /**
     * @Route("/admin/exposition/remove/{id}", name="admin.exposition.remove")
     */
    public function remove(ExpositionsRepository $expositionsRepository, EntityManagerInterface $entityManager, int $id, FileService $fileService):Response
    {
        $model = $expositionsRepository->find($id);

        $entityManager->remove($model);
        $entityManager->flush();

        $this->addFlash('notice', "L'exposition a été supprimée");
        return $this->redirectToRoute('admin.exposition.index');
    }
}