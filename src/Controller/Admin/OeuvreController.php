<?php

namespace App\Controller\Admin;

use App\Entity\Expositions;
use App\Entity\Oeuvre;
use App\Form\ExpositionType;
use App\Form\OeuvreType;
use App\Repository\OeuvreRepository;
use App\Service\FileService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OeuvreController extends AbstractController
{
    /**
     * @Route("/admin/oeuvre", name="admin.oeuvre.index")
     */
    public function index(OeuvreRepository $oeuvreRepository):Response
    {
        $results = $oeuvreRepository->findAll();

        return $this->render('admin/oeuvre/index.html.twig', [
            'results' => $results
        ]);
    }
    /**
     * @Route("/admin/oeuvre/update/{id}", name="admin.oeuvre.update")
     */
    public function form(Request $request, EntityManagerInterface $entityManager, int $id, OeuvreRepository $oeuvreRepository):Response
    {
        $results = $oeuvreRepository->findOneBy([
            'id' => $id
        ]);

        $model = $oeuvreRepository->find($id);
        $type = OeuvreType::class;

//        $form = $this->createForm($type, $model);
        $form = $this->createForm($type, $results);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){


            // message de confirmation
            $message = "L'oeuvre a été modifiée";

            // message stocké en session
            $this->addFlash('notice', $message);

            /*
             * insertion dans la base de données
             *  - persist: méthode déclenchée uniquement lors d'une insertion
             *  - lors d'une mise à jour, aucune méthode n'est requise
             *  - remove: méthode déclenchée uniquement lors d'une suppression
             *  - flush: exécution des requêtes SQL
             */
            $model->getId() ? null : $entityManager->persist($model);
            $entityManager->flush();

            // redirection
            return $this->redirectToRoute('admin.oeuvre.index');
        }

        return$this->render('admin/oeuvre/form.html.twig', [
            'form' => $form->createView(),
            'results' => $results
        ]);
    }
    /**
     * @Route("/admin/oeuvre/create", name="admin.oeuvre.create")
     */
    public function create(Request $request, EntityManagerInterface $entityManager, OeuvreRepository $oeuvreRepository):Response
    {
        $results = $oeuvreRepository->findAll();
        // si l'id est nul, une insertion est exécutée, sinon une modification est exécutée
        $model = new Oeuvre();
        $type = OeuvreType::class;
        $form = $this->createForm($type, $model);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {


            // message de confirmation
            $message = "L'oeuvre a été ajoutée";

            // message stocké en session
            $this->addFlash('notice', $message);

            /*
             * insertion dans la base de données
             *  - persist: méthode déclenchée uniquement lors d'une insertion
             *  - lors d'une mise à jour, aucune méthode n'est requise
             *  - remove: méthode déclenchée uniquement lors d'une suppression
             *  - flush: exécution des requêtes SQL
             */
            $entityManager->persist($model);
            $entityManager->flush();

            // redirection
            return $this->redirectToRoute('admin.oeuvre.index');
        }
        return$this->render('admin/oeuvre/create.form.html.twig', [
            'form' => $form->createView()
//            'results' => $results
        ]);
    }

    /**
     * @Route("/admin/oeuvre/remove/{id}", name="admin.oeuvre.remove")
     */
    public function remove(OeuvreRepository $oeuvreRepository, EntityManagerInterface $entityManager, int $id, FileService $fileService):Response
    {
        $model = $oeuvreRepository->find($id);

        $entityManager->remove($model);
        $entityManager->flush();

        if(file_exists("img/oeuvre/{$model->getImage()}")){
            $fileService->remove('img/oeuvre', $model->getImage());
        }

        $this->addFlash('notice', "L'oeuvre a été supprimée");
        return $this->redirectToRoute('admin.oeuvre.index');
    }
}