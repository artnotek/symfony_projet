<?php

namespace App\Repository;

use App\Entity\Oeuvre;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Oeuvre|null find($id, $lockMode = null, $lockVersion = null)
 * @method Oeuvre|null findOneBy(array $criteria, array $orderBy = null)
 * @method Oeuvre[]    findAll()
 * @method Oeuvre[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OeuvreRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Oeuvre::class);
    }
    public function getOeuvre()
    {
        return $this->createQueryBuilder('oeuvre')
            ->andWhere('exposition.date >= :date')
            ->setParameter('date', new \DateTime())
            ->orderBy('exposition.date', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }
//    public function testDQL():Query
//    {
//        $query = $this->createQueryBuilder('oeuvre')
//
//            ->select('categories.name,
//				GROUP_CONCAT(product.id) groupid,
//				COUNT(product.id) AS total'
//            )
//            ->join('product.categories', 'categories')
//            ->groupBy('categories.name')
//            ->orderBy('total', 'DESC')
//            ->getQuery()
//        ;
//
//        // retour de la requête
//        return $query;
//    }


    // /**
    //  * @return Oeuvre[] Returns an array of Oeuvre objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Oeuvre
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
